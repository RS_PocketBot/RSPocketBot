import sys
import traceback
import discord
from discord.ext import commands
from pyrspb.objects.database import Base, engine
from os.path import basename
import time

Base.metadata.create_all(engine)
bot = commands.Bot(command_prefix="!")


extensions = ['cogs.invention', "cogs.suitytest", "cogs.reload"]


for extension in extensions:
    try:
        bot.load_extension(extension)
        time.sleep(1)
    except Exception as e:
        print('Failed to load {}'.format(extension), file=sys.stderr)
        traceback.print_exc()


@bot.event
async def on_ready():
    print('Logged in as: {} - {}\nVersion: {}\n'.format(bot.user.name, bot.user.id, discord.__version__))
    await bot.change_presence(status="Treasure Hunter")

with open("token", "r") as token:
    data = token.readlines().pop()
    print(bot.extensions)
    bot.run(data, reconnect=True)

