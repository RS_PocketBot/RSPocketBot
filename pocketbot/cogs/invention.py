import discord
from discord.ext import commands
from pyrspb.api.invention import material_calculator
from prettytable import PrettyTable
from pyrspb.objects.invention import MaterialSource


def item_to_list(item: MaterialSource):
    return [item.name, item.buy_limit, item.materials_per_hour, item.cost_per_material]


class InventionCog:
    def __init__(self, bot: discord.Client):
        self.bot = bot  # type: discord.Client

    @commands.command()
    async def component(self, ctx: commands.Context, *, material_string: str):
        material = material_calculator.material_from_string(material_string)
        if material.rarity == "Rare":
            await ctx.send(material.full_list_url)
        else:
            items = material_calculator.identify_ideal_sources(material)[:8]
            table = PrettyTable(["Name", "Buy limit", "Materials/hr", "Gp/material"])
            for item in items:
                table.add_row(item_to_list(item))
            await ctx.send(str("```\n" + str(table) + "```"))


def setup(bot):
    bot.add_cog(InventionCog(bot))
