from discord.ext import commands


class ReloadCog:
    def __init__(self, bot):
        self.bot = bot

    def reload_cog(self, cog_name):
        try:
            print(self.bot)
            print(cog_name)
            self.bot.unload_extension(cog_name)
            self.bot.load_extension(cog_name)
        except Exception as e:
            print(e)

    @commands.command()
    async def r(self, ctx: commands.Context):
        map(self.reload_cog, ["cogs.invention", "cogs.suitytest"])
        await ctx.send("Reloaded.")


def setup(bot):
    bot.add_cog(ReloadCog(bot))
