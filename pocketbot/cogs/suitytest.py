import discord
from discord.ext import commands


class SuityCog:
    def __init__(self, bot: discord.Client):
        self.bot = bot  # type: discord.Client

    @commands.command()
    async def suittest(self, ctx: commands.Context):
        await ctx.send("This is a Suit Test command.")


def setup(bot):
    bot.add_cog(SuityCog(bot))
